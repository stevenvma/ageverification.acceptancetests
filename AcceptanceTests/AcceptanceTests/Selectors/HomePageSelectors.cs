﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcceptanceTests.Selectors
{
    public class HomePageSelectors
    {
        public SelectorActions Title { get; } = new SelectorActions("//div[@class='text-center']//h1[@class='display-4']");
        public SelectorActions LoggedInText { get; } = new SelectorActions("//div[@class='text-center']//p[contains(text(), 'Hello. You are logged in as ')]");
        public SelectorActions UserEmailText { get; } = new SelectorActions("//div[@class='text-center']//span[@class='text-dark']");
        public SelectorActions InstructionText { get; } = new SelectorActions("//div[@class='text-center']//p[contains(text(), 'Please')]");
        public SelectorActions LoginLink { get; } = new SelectorActions("//div[@class='text-center']//a[@href='/Identity/Account/Login']");
        public SelectorActions RegisterLink { get; } = new SelectorActions("//div[@class='text-center']//a[@href='/Identity/Account/Register']");
        public SelectorActions LoginHistoryLink { get; } = new SelectorActions("//div[@class='text-center']//a[@href='/LoginHistory']");
        public SelectorActions LoginStatsLink { get; } = new SelectorActions("//div[@class='text-center']//a[@href='/LoginHistory/Chart']");

}
}
