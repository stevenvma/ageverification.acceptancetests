﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcceptanceTests.Selectors
{
    public class PasswordPageSelectors
    {
        public SelectorActions Title { get; } = new SelectorActions("//main[@class='pb-3']/h1[1]");
        public SelectorActions InstructionText { get; } = new SelectorActions("//form[@id='account']/h4[1]");
        public SelectorActions SpecificationText { get; } = new SelectorActions("//form[@id='account']/p[1]");
        public SelectorActions ValidationSummaryError01 { get; } = new SelectorActions("//div[@class='text-danger validation-summary-errors']//li[1]");
        public SelectorActions ValidationSummaryError02 { get; } = new SelectorActions("//div[@class='text-danger validation-summary-errors']//li[2]");
        public SelectorActions PasswordLabel { get; } = new SelectorActions("//div[@class='form-group']//label[@for='Input_Password']");
        public SelectorActions PasswordField { get; } = new SelectorActions("//div[@class='form-group']//input[@id='Input_Password']");
        public SelectorActions PasswordError { get; } = new SelectorActions("//span[@class='text-danger field-validation-error']/span[@id='Input_Password-error']");
        public SelectorActions ConfirmPasswordLabel { get; } = new SelectorActions("//div[@class='form-group']//label[@for='Input_ConfirmPassword']");
        public SelectorActions ConfirmPasswordField { get; } = new SelectorActions("//div[@class='form-group']//input[@id='Input_ConfirmPassword']");
        public SelectorActions ConfirmPasswordError { get; } = new SelectorActions("//span[@class='text-danger field-validation-error']/span[@id='Input_ConfirmPassword-error']");
        public SelectorActions CompleteButton { get; } = new SelectorActions("//div[@class='form-group']/button[@type='submit']");
    }
}
