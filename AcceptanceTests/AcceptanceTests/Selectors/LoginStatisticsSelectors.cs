﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcceptanceTests.Selectors
{
    public class LoginStatisticsSelectors
    {
        public SelectorActions Title { get; } = new SelectorActions("//main[@class='pb-3']/h1[1]");
        public SelectorActions LoginChart { get; } = new SelectorActions("//main[@class='pb-3']//canvas[@id='chart']");
    }
}
