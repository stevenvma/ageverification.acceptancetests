﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcceptanceTests.Selectors
{
    public class SavedTestData
    {
        public string UserName;
        public string UserEmail;
        public string UserDob;
        public string UserPassword;
        public string LoginSuccessDateTime01;
        public string LoginSuccessDateTime02;
        public string LoginFailureDateTime01;
    }
}
