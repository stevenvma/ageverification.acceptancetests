﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcceptanceTests.Selectors
{
    public class LoginHistorySelectors
    {
        public SelectorActions Title { get; } = new SelectorActions("//main[@role='main']//h3[1]");
        public SelectorActions RecordTable { get; } = new SelectorActions("//main[@role='main']//table[@class='table responsive-table table-striped']");
        public SelectorActions FilterButton { get; } = new SelectorActions("NO SUCH ELEMENT");
        public SelectorActions DateColumnLabel { get; } = new SelectorActions("//table[@class='table responsive-table table-striped']//thead[1]//th[1]");
        public SelectorActions SuccessColumnLabel { get; } = new SelectorActions("//table[@class='table responsive-table table-striped']//thead[1]//th[2]");
        public SelectorActions RecordDate(int recordNumber) => new SelectorActions($"//table[@class='table responsive-table table-striped']//tbody[1]//tr[{recordNumber}]//td[1]");
        public SelectorActions RecordSuccess(int recordNumber) => new SelectorActions($"//table[@class='table responsive-table table-striped']//tbody[1]//tr[{recordNumber}]//td[2]");

    }
}
