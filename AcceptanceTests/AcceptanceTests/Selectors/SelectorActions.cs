﻿using AcceptanceTests.Drivers;
using System;
using System.Collections.Generic;
using System.Text;

namespace AcceptanceTests.Selectors
{
    public class SelectorActions
    {
        public string XPath { get; }
        public SelectorActions(string xPath)
        {
            XPath = xPath;
        }

        public void Click()
        {
            WebDriverFactory.Instance.Click(XPath);
        }

        public void EnterText(string inputText)
        {
            WebDriverFactory.Instance.EnterText(XPath, inputText);
        }

        public void VerifyExists()
        {
            WebDriverFactory.Instance.VerifyExists(XPath);
        }

        public void VerifyTextContains(string expectedText)
        {
            WebDriverFactory.Instance.VerifyTextContains(XPath, expectedText);
        }
    }
}
