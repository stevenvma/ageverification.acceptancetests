﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcceptanceTests.Selectors
{
    public class RegisterPageSelectors
    {
        public SelectorActions Title { get; } = new SelectorActions("//main[@class='pb-3']/h1[1]");
        public SelectorActions InstructionText { get; } = new SelectorActions("//form[@method='post']/h4[1]");
        public SelectorActions ValidationSummaryError01 { get; } = new SelectorActions("//div[@class='text-danger validation-summary-errors']//li[1]");
        public SelectorActions ValidationSummaryError02 { get; } = new SelectorActions("//div[@class='text-danger validation-summary-errors']//li[2]");
        public SelectorActions EmailLabel { get; } = new SelectorActions("//div[@class='form-group']//label[@for='Input_Email']");
        public SelectorActions EmailField { get; } = new SelectorActions("//div[@class='form-group']//input[@id='Input_Email']");
        public SelectorActions EmailError { get; } = new SelectorActions("//span[@class='text-danger field-validation-error']/span[@id='Input_Email-error']");
        public SelectorActions NameLabel { get; } = new SelectorActions("//div[@class='form-group']//label[@for='Input_Name']");
        public SelectorActions NameField { get; } = new SelectorActions("//div[@class='form-group']//input[@id='Input_Name']");
        public SelectorActions NameError { get; } = new SelectorActions("//span[@class='text-danger field-validation-error']/span[@id='Input_Name-error']");
        public SelectorActions DateOfBirthLabel { get; } = new SelectorActions("//div[@class='form-group']//label[@for='Input_DateOfBirth']");
        public SelectorActions DateOfBirthField { get; } = new SelectorActions("//div[@class='form-group']//input[@id='Input_DateOfBirth']");
        public SelectorActions DateOfBirthError { get; } = new SelectorActions("//span[@class='text-danger field-validation-error']/span[@id='Input_DateOfBirth-error']");
        public SelectorActions RegisterButton { get; } = new SelectorActions("//form[@method='post']//button[@type='submit']");
    }
}
