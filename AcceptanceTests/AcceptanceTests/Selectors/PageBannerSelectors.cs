﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcceptanceTests.Selectors
{
    public class PageBannerSelectors
    {
        public SelectorActions AppName { get; } = new SelectorActions("//div[@class='container']//a[@class='navbar-brand'][text()='AgeVerificationExample.Web']");
        public SelectorActions HomeButton { get; } = new SelectorActions("//li[@class='nav-item']//a[@class='nav-link text-dark'][text()='Home']");
        public SelectorActions RegisterButton { get; } = new SelectorActions("//li[@class='nav-item']//a[@class='nav-link text-dark'][text()='Register']");
        public SelectorActions LoginButton { get; } = new SelectorActions("//li[@class='nav-item']//a[@class='nav-link text-dark'][text()='Login']");
        public SelectorActions LoginHistoryButton { get; } = new SelectorActions("//li[@class='nav-item']//a[@class='nav-link text-dark'][text()='View Login History']");
        public SelectorActions LogoutButton { get; } = new SelectorActions("//li[@class='nav-item']//button[@class='nav-link btn btn-link text-dark'][text()='Logout']");
}
}
