﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcceptanceTests.Selectors
{
    public class SelectorDirectory
    {
        public SavedTestData SavedTestData = new SavedTestData();

        public PageBannerSelectors PageBanner = new PageBannerSelectors();
        public HomePageSelectors HomePage { get; } = new HomePageSelectors();
        public LoginPageSelectors LoginPage { get; } = new LoginPageSelectors();
        public RegisterPageSelectors RegisterPage { get; } = new RegisterPageSelectors();
        public PasswordPageSelectors PasswordEntryPage { get; } = new PasswordPageSelectors();
        public LoginHistorySelectors LoginHistoryPage { get; } = new LoginHistorySelectors();
        public LoginStatisticsSelectors LoginStatisticsPage { get; } = new LoginStatisticsSelectors();
    }
}
