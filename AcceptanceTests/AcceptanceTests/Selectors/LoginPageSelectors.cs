﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcceptanceTests.Selectors
{
    public class LoginPageSelectors
    {
        public SelectorActions Title { get; } = new SelectorActions("//main[@class='pb-3']/h1[1]");
        public SelectorActions InstructionText { get; } = new SelectorActions("//form[@id='account']/h4[1]");
        public SelectorActions ValidationSummaryError01 { get; } = new SelectorActions("//div[@class='text-danger validation-summary-errors']//li[1]");
        public SelectorActions ValidationSummaryError02 { get; } = new SelectorActions("//div[@class='text-danger validation-summary-errors']//li[2]");
        public SelectorActions EmailLabel { get; } = new SelectorActions("//div[@class='form-group']//label[@for='Input_Email']");
        public SelectorActions EmailField { get; } = new SelectorActions("//div[@class='form-group']//input[@id='Input_Email']");
        public SelectorActions EmailError { get; } = new SelectorActions("//span[@class='text-danger field-validation-error']/span[@id='Input_Email-error']");
        public SelectorActions PasswordLabel { get; } = new SelectorActions("//div[@class='form-group']//label[@for='Input_Password']");
        public SelectorActions PasswordField { get; } = new SelectorActions("//div[@class='form-group']//input[@id='Input_Password']");
        public SelectorActions PasswordError { get; } = new SelectorActions("//span[@class='text-danger field-validation-error']/span[@id='Input_Password-error']");
        public SelectorActions RememberMe { get; } = new SelectorActions("//div[@class='checkbox']/label[@for='Input_RememberMe']");
        public SelectorActions LoginButton { get; } = new SelectorActions("//div[@class='form-group']//button[@type='submit']");
        public SelectorActions RegisterLink { get; } = new SelectorActions("//div[@class='form-group']//a[@href='/Identity/Account/Register?returnUrl=%2F']");
        public SelectorActions LoginStatsLink { get; } = new SelectorActions("//div[@class='text-center']//a[@href='/LoginHistory/Chart')]");
    }
}
