﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Collections.Generic;
using System.Text;

namespace AcceptanceTests.Drivers
{
    public static class WebDriverFactory
    {
        public static readonly WebDriverSelenium Instance = new WebDriverSelenium();


        public static IWebDriver SetDriver()
        {
#if Chrome
            ChromeDriver _driver = new ChromeDriver();
#elif IE
            InternetExplorerDriver _driver = new InternetExplorerDriver();
#elif Firefox
            FirefoxOptions profile = new FirefoxOptions();
            profile.AcceptInsecureCertificates = true;
            FirefoxDriver _driver = new FirefoxDriver(profile);
#endif
            return _driver;
        }
    }
}
