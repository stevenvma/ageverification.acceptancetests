﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;

namespace AcceptanceTests.Drivers
{
    public class WebDriverSelenium
    {
        public IWebDriver Driver { get; set; }
        
        public void StartSession()
        {
            Driver = WebDriverFactory.SetDriver();

            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
            Driver.Manage().Window.Maximize();

            Driver.Navigate().GoToUrl("https://localhost:44347/");
        }

        public void EndSession()
        {
            Driver?.Quit();
            Driver?.Dispose();
        }

        /// <summary>
        /// Clicks on first element that matches the provided xPath
        /// </summary>
        /// <param name="xPath"></param>
        public void Click(string xPath)
        {
            FindElement(xPath).Click();           
        }

        /// <summary>
        /// Clears, then enters given string of characters into an element that matches the provided xPath
        /// </summary>
        /// <param name="xPath"></param>
        /// <param name="inputText"></param>
        public void EnterText(string xPath, string inputText)
        {
            var element = FindElement(xPath);
            element.Clear();
            element.SendKeys(inputText);            
        }

        /// <summary>
        /// Verifies that an element exists that matches the given xPath
        /// </summary>
        /// <param name="xPath"></param>
        public void VerifyExists(string xPath)
        {
            Assert.IsTrue(FindElement(xPath).Displayed, $"No visible element was found with xPath: '{xPath}'.");
        }

        /// <summary>
        /// Verifies that the text of a given element contains that which we expect
        /// </summary>
        /// <param name="xPath"></param>
        /// <param name="expectedText"></param>
        public void VerifyTextContains(string xPath, string expectedText)
        {
            string actualText = FindElement(xPath).Text;
            Assert.IsTrue(actualText.Contains(expectedText), $"Element found had text '{actualText}', not expected text of '{expectedText}'.");
        }

        /// <summary>
        /// Waits until an element is present with a matching xPath, for a given timeout
        /// </summary>
        /// <param name="xPath"></param>
        /// <returns></returns>
        private IWebElement FindElement (string xPath)
        {
            try
            {
                return new WebDriverWait(Driver, TimeSpan.FromSeconds(7)).Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(xPath)));
            }
            catch
            {
                throw new Exception($"No element was found, with XPath: '{xPath}'.");
            }
        }
    }
}
