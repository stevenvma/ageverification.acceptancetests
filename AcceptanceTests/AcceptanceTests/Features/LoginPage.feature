﻿Feature: LoginPage
	As an Age Verification user
	I want to be able to login or register correctly
	So that I can gain valid access to the application

Scenario: It is possible to access the Register Page from the Login Page
	Given the Login Page is displayed for a guest
	When the Register Link is clicked
	Then the Registration Page is displayed

Scenario: It is possible to log in as a registered user
	Given there is a valid registered user
	And the Login Page is displayed for a guest
	When details for a registered user are entered
	And the Log in button is clicked
	Then the Home Page is displayed for a registered user

Scenario: It is not possible to log in with the incorrect password, as a registered user
	Given there is a valid registered user
	And the Login Page is displayed for a guest
	When a valid user email, but an incorrect password, are entered
	And the Log in button is clicked
	Then the Login Page is displayed
	And an error message states that the login details are incorrect