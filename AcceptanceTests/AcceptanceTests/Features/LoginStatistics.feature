﻿Feature: LoginStatistics
	As an Age Verification user
	I want to view privacy conscience statistics for successful and failing login attempts to the application
	So that I can get an idea of application usage

@Requirement
Scenario: The Login Statistics Page displays all required elements
	Given the Login Statistics Page is displayed for a registered user
	Then the login statistics page contains all required elements

@Requirement
Scenario: The Login Statistics chart displays login data from the past 30 days for all users
	Given the Login Statistics Page is displayed for a registered user
	Then 30 days of login data is displayed, grouped by success or failure 

