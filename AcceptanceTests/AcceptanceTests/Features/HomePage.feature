﻿Feature: HomePage
	As an Age Verification user
	I want a Home Page
	So that I have location from which to begin my login or registration

Scenario: The Home Page must contain all required elements for a guest
	Given the Home Page is displayed for a guest
	Then all home page elements required for a guest are present

Scenario: It is possible to access the Login Page form the Home Page
	Given the Home Page is displayed for a guest
	When the Login link is cicked
	Then the Login Page is displayed

Scenario: The Home Page must contain all required elements for a registered user
	Given the Home Page is displayed for a registered user
	Then all home page elements required for a registered user are present