﻿Feature: PasswordPage
	As a user of the Age Verification web application
	I want to assign a password to my account
	So that I am able to log in with my details securely

Scenario: The Password Entry page must contain all required elements
	Given the Password Entry page is displayed for a guest
	Then the Password Entry page must contain all required assets

@Requirement
Scenario Outline: Registration must fail if the password entered does not meet the complexity requirements
	Given the Password Entry page is displayed for a guest
	When an invalid password is entered, that doesn't have <PasswordRequirement>
	And the complete registration button is clicked
	Then an error message is displayed warning that the password does not have <PasswordRequirement>
	Examples: 
	| PasswordRequirement |
	| 12+ characters      |
	| 1 upper case        |
	| 1 lower case        |
	| 1 number            |
	| 1 special character |

@Requirement
Scenario: Registration must complete if the password is valid and the confirmation matches
	Given the Password Entry page is displayed for a guest
	When a valid password and password confirmation is entered
	And the complete registration button is clicked
	Then the Login Page is displayed