﻿Feature: LoginHistory
	As an Age Verification user
	I want to a record of login attempts for my account
	So that I can see the date and time of successful and failed login attempts

Scenario: The Login History page contains all required elements
Given the Login History page is displayed for a registered user
Then the Login History page contains all required elements

@Requirement
Scenario: The Login History page displays records of successful login attempts
Given a registered user has logged into their account at a certain time
And the registered user logged out of their account
And a registered user has re-logged into their account at a certain time
When the Login History link is clicked
Then the Login History page displays the user's first successful login
And the Login History page displays the user's second successful login

@Requirement
Scenario: The Login History page displays records of unsuccessful login attempts
Given a registered user fails to log into their account at a certain time
And a registered user has re-logged into their account at a certain time
When the Login History link is clicked
Then the Login History page displayed the user's first failed login
And the Login History page displays the user's second successful login

# This test fails as there is no means by which to sort login table entries, despite the requirement
@Requirement
Scenario: It is possible to sort login history records
Given a registered user has logged into their account at a certain time
And the registered user logged out of their account
And a registered user has re-logged into their account at a certain time
And the Login History link is clicked
When the date column header is clicked
Then the login history is sorted by date ascending

#This test fails as there is no means by which to filter login histroy results, despite the requirement
@Requirement
Scenario: It is possible to filter login history records
Given a registered user fails to log into their account at a certain time
And a registered user has re-logged into their account at a certain time
And the Login History link is clicked
When the login history table is filtered for failures
Then only login failures are displayed