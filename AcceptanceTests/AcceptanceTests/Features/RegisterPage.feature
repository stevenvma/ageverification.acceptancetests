﻿Feature: Register Page
	As a user of the Age Verification app, over 18 years of age
	I want to register my details
	So that I can create a user account

Scenario: The Register Page must contain all required elements
	Given the Register Page is displayed for a guest
	Then the Register Page contains all required elements

@Requirement
Scenario: Registration must fail without a valid Email
	Given the Register Page is displayed for a guest
	When the Register button is clicked without a valid Email
	Then registration does not proceed
	And an error message states the need for an Email

@Requirement
Scenario: Registration must fail without a valid Name
	Given the Register Page is displayed for a guest
	When the Register button is clicked without a valid Name
	Then registration does not proceed
	And an error message states the need for an Name

@Requirement
Scenario: Registration must fail without a valid Date of Birth
	Given the Register Page is displayed for a guest
	When the Register button is clicked without a valid Date of Birth
	Then registration does not proceed
	And an error message states the need for an Date of Birth

@Requirement
Scenario: Registration must warn on input of an invalid Email
	Given the Register Page is displayed for a guest
	When the Register button is clicked with an invalid Email
	Then an error messages states that the Email is invalid

@Requirement
Scenario: Registration must proceed, to Password entry, with valid Name, Email and DoB
	Given the Register Page is displayed for a guest
	When the Register button is clicked with a valid Name, Email and DoB
	Then the Password Entry Page is displayed

@Requirement
Scenario: Registration must be locked after 3 failures with the same Name and Email
	Given the Register Page is displayed for a guest
	When the Register button is clicked 3 times with an invalid Date of Birth
	Then an error message states that registration is locked for those details
	And it is not possible to register those same details with a valid Date of Birth