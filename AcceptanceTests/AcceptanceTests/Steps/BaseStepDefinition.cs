﻿using AcceptanceTests.Selectors;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace AcceptanceTests.Steps
{
    public class BaseStepDefinition : TechTalk.SpecFlow.Steps
    {
        protected SelectorDirectory App { get; } = new SelectorDirectory();

        protected TSteps Resolve<TSteps>()
    where TSteps : TechTalk.SpecFlow.Steps
        {
            return ScenarioContext.ScenarioContainer.Resolve<TSteps>();
        }
    }
}
