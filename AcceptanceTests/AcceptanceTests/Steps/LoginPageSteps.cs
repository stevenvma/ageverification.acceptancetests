﻿using AcceptanceTests.Drivers;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;

namespace AcceptanceTests.Steps
{
    [Binding]
    public class LoginPageSteps : BaseStepDefinition
    {
        #region Given
        
        [Given(@"the Login Page is displayed for a guest")]
        public void GivenTheLoginPageIsDisplayedForAGuest()
        {
            var homePage = Resolve<HomePageSteps>();

            homePage.GivenTheHomePageIsDisplayedForA("guest");
            homePage.WhenTheLoginLinkIsCicked();
            ThenTheLoginPageIsDisplayed();
        }

        #endregion

        #region When
        [When(@"the Register Link is clicked")]
        public void WhenTheRegisterLinkIsClicked()
        {
            App.LoginPage.RegisterLink.Click();
        }

        [When(@"details for a registered user are entered")]
        public void WhenDetailsForARegisteredUserAreEntered()
        {
            var userEmail = ScenarioContext.Get<string>(nameof(App.SavedTestData.UserEmail));
            var userPassword = ScenarioContext.Get<string>(nameof(App.SavedTestData.UserPassword));

            App.LoginPage.EmailField.EnterText(userEmail);
            App.LoginPage.PasswordField.EnterText(userPassword);
        }

        [When(@"the Log in button is clicked")]
        public void WhenTheLogInButtonIsClicked()
        {
            App.LoginPage.LoginButton.Click();
        }

        [When(@"a valid user email, but an incorrect password, are entered")]
        public void WhenAValidUserEmailButAnIncorrectPasswordAreEntered()
        {
            var userEmail = ScenarioContext.Get<string>(nameof(App.SavedTestData.UserEmail));

            App.LoginPage.EmailField.EnterText(userEmail);
            App.LoginPage.PasswordField.EnterText("incorrect password");
        }


        #endregion

        #region Then

        [Then(@"the Login Page is displayed")]
        public void ThenTheLoginPageIsDisplayed()
        {
            App.LoginPage.RememberMe.VerifyExists();
        }

        [Then(@"an error message states that the login details are incorrect")]
        public void ThenAnErrorMessageStatesThatTheLoginDetailsAreIncorrect()
        {
            App.LoginPage.ValidationSummaryError01.VerifyTextContains("Invalid login attempt.");
        }

        #endregion
    }
}