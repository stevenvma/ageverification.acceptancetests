﻿using AcceptanceTests.Drivers;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcceptanceTests.Steps
{
    public class HelperMethods : BaseStepDefinition
    {
        /// <summary>
        /// Use when the current time is about to be recorded for an action in the test, to ensure that the recorded time, and the time of the actual action definitely happen within the same 10 second span
        /// This is so when time is recorded, it can be verified to within 10 seconds of the actual time
        /// </summary>
        public void EnsureThatEnoughTimeRemainsInTheCurrentInterval()
        {
            //interval = 10 second, eg: 40 - 50
            int secondsRemainingInCurrentInterval = 10 - DateTime.Now.Second % 10;
            while (secondsRemainingInCurrentInterval < 3)
            {
                secondsRemainingInCurrentInterval = 10 - DateTime.Now.Second % 10;
                // ie: wait
            }
        }

        /// <summary>
        /// Use when recording the time of a second, distinct action, to ensure it happened at a different Date Time/different minute
        /// </summary>
        public void EnsureThatEnoughTimeHasPassedSinceAPreviouslyRecordedAction()
        {
            int startingInterval = DateTime.Now.Second / 10;
            int currentInterval = DateTime.Now.Second / 10;
            while (currentInterval == startingInterval)
            {
                currentInterval = DateTime.Now.Second / 10;
                //ie:  wait
            }
        }

        public string RecordCurrentDateTimeToWithin10SecondInterval()
        {
            return System.DateTime.UtcNow.ToString("dd/MM/yyyy hh:mm:ss").Substring(0, 18);
        }
    }
}
