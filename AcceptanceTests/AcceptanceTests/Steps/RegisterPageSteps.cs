﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace AcceptanceTests.Steps
{
    [Binding]
    public class RegisterPageSteps : BaseStepDefinition
    {
        #region Given

        [Given(@"the Register Page is displayed for a guest")]
        public void GivenTheRegisterPageIsDisplayedForAGuest()
        {
            var loginPage = Resolve<LoginPageSteps>();

            loginPage.GivenTheLoginPageIsDisplayedForAGuest();
            loginPage.WhenTheRegisterLinkIsClicked();

            ThenTheRegistrationPageIsDisplayed();
        }

        #endregion

        #region When

        [When(@"the Register button is clicked without a valid Email")]
        public void WhenTheRegisterButtonIsClickedWithoutAValidEmail()
        {
            App.RegisterPage.NameField.EnterText("John McName");
            App.RegisterPage.DateOfBirthField.EnterText("01/01/2000");
            App.RegisterPage.RegisterButton.Click();
        }

        [When(@"the Register button is clicked without a valid Name")]
        public void WhenTheRegisterButtonIsClickedWithoutAValidName()
        {
            App.RegisterPage.EmailField.EnterText("McName@Test.com");
            App.RegisterPage.DateOfBirthField.EnterText("01/01/2000");
            App.RegisterPage.RegisterButton.Click();
        }

        [When(@"the Register button is clicked without a valid Date of Birth")]
        public void WhenTheRegisterButtonIsClickedWithoutAValidDateOfBirth()
        {
            App.RegisterPage.NameField.EnterText("John McName");
            App.RegisterPage.EmailField.EnterText("McName@Test.com");
            App.RegisterPage.RegisterButton.Click();
        }

        [When(@"the Register button is clicked with an invalid Email")]
        public void WhenTheRegisterButtonIsClickedWithAnInvalidEmail()
        {
            App.RegisterPage.EmailField.EnterText("Invalid");
            App.RegisterPage.NameField.EnterText("John McName");
            App.RegisterPage.DateOfBirthField.EnterText("01/01/2000");
            App.RegisterPage.RegisterButton.Click();
        }

        [When(@"the Register button is clicked with a valid Name, Email and DoB")]
        public void WhenTheRegisterButtonIsClickedWithAValidNameEmailAndDoB()
        {
            string UniqueIdentifier = System.Guid.NewGuid().ToString();
            var UserEmail = UniqueIdentifier + "@Test.com";
            var UserName = "John" + UniqueIdentifier;
            var UserDob = "01/01/2000";
            ScenarioContext.Set(UserName, nameof(App.SavedTestData.UserName)); //recording this for potential later use in same scenario
            ScenarioContext.Set(UserEmail, nameof(App.SavedTestData.UserEmail)); //recording this for potential later use in same scenario
            ScenarioContext.Set(UserDob, nameof(App.SavedTestData.UserDob)); //recording this for potential later use in same scenario

            App.RegisterPage.EmailField.EnterText(UserEmail);
            App.RegisterPage.NameField.EnterText(UserName);
            App.RegisterPage.DateOfBirthField.EnterText(UserDob);
            App.RegisterPage.RegisterButton.Click();
        }

        [When(@"the Register button is clicked 3 times with an invalid Date of Birth")]
        public void WhenTheRegisterButtonIsClicked3TimesWithAnInvalidDateOfBirth()
        {
            string UnderAgeDob = System.DateTime.Now.AddYears(-16).Date.ToString("dd/MM/yyyy");
            string UniqueIdentifier = System.Guid.NewGuid().ToString();

            App.RegisterPage.EmailField.EnterText("McName@Test.com");
            App.RegisterPage.NameField.EnterText(UniqueIdentifier + "McName");
            App.RegisterPage.DateOfBirthField.EnterText(UnderAgeDob);
            
            App.RegisterPage.RegisterButton.Click();
            App.RegisterPage.ValidationSummaryError01.VerifyTextContains("You must be at least 18 to register");

            App.RegisterPage.RegisterButton.Click();
            App.RegisterPage.ValidationSummaryError01.VerifyTextContains("You must be at least 18 to register");

            App.RegisterPage.RegisterButton.Click();
        }

        #endregion

        #region Then

        [Then(@"the Registration Page is displayed")]
        public void ThenTheRegistrationPageIsDisplayed()
        {
            App.RegisterPage.NameField.VerifyExists();
        }

        [Then(@"the Register Page contains all required elements")]
        public void ThenTheRegisterPageContainsAllRequiredElements()
        {
            var register = App.RegisterPage;

            register.Title.VerifyTextContains("Register");
            register.InstructionText.VerifyTextContains("Create a new account.");
            register.EmailLabel.VerifyTextContains("Email");
            register.EmailField.VerifyExists();
            register.NameLabel.VerifyTextContains("Name");
            register.NameField.VerifyExists();
            register.DateOfBirthLabel.VerifyTextContains("Date Of Birth");
            register.DateOfBirthField.VerifyExists();
            register.RegisterButton.VerifyTextContains("Register");
        }

        [Then(@"registration does not proceed")]
        public void ThenRegistrationDoesNotProceed()
        {
            ThenTheRegistrationPageIsDisplayed();
        }

        [Then(@"an error message states the need for an Email")]
        public void ThenAnErrorMessageStatesTheNeedForAnEmails()
        {
            App.RegisterPage.ValidationSummaryError01.VerifyTextContains("The Email field is required.");
            App.RegisterPage.EmailError.VerifyTextContains("The Email field is required.");
        }

        [Then(@"an error message states the need for an Name")]
        public void ThenAnErrorMessageStatesTheNeedForAnName()
        {
            App.RegisterPage.ValidationSummaryError01.VerifyTextContains("The Name field is required.");
            App.RegisterPage.NameError.VerifyTextContains("The Name field is required.");
        }

        [Then(@"an error message states the need for an Date of Birth")]
        public void ThenAnErrorMessageStatesTheNeedForAnDateOfBirth()
        {
            App.RegisterPage.ValidationSummaryError01.VerifyTextContains("The Date Of Birth field is required.");
            App.RegisterPage.DateOfBirthError.VerifyTextContains("The Date Of Birth field is required.");
        }

        [Then(@"an error messages states that the Email is invalid")]
        public void ThenAnErrorMessagesStatesThatTheEmailIsInvalid()
        {
            App.RegisterPage.ValidationSummaryError01.VerifyTextContains("The Email field is not a valid e-mail address.");
            App.RegisterPage.EmailError.VerifyTextContains("The Email field is not a valid e-mail address.");
        }

        [Then(@"an error message states that registration is locked for those details")]
        public void ThenAnErrorMessageStatesThatRegistrationIsLockedForThoseDetails()
        {
            App.RegisterPage.ValidationSummaryError01.VerifyTextContains("You must be at least 18 to register");
            App.RegisterPage.ValidationSummaryError02.VerifyTextContains("Your registration details have been locked out due to multiple registration failures.");
        }

        [Then(@"it is not possible to register those same details with a valid Date of Birth")]
        public void ThenItIsNotPossibleToRegisterThoseSameDetailsWithAValidDateOfBirth()
        {
            string OverAgeDob = System.DateTime.Now.AddYears(+1).Date.ToString("dd/MM/yyyy");

            App.RegisterPage.DateOfBirthField.EnterText(OverAgeDob);
            App.RegisterPage.RegisterButton.Click();

            App.RegisterPage.ValidationSummaryError01.VerifyTextContains("Your registration details have been locked out due to multiple registration failures.");
        }


        #endregion
    }
}
