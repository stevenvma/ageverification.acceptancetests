﻿using AcceptanceTests.Drivers;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;

namespace AcceptanceTests.Steps
{
    [Binding]
    public class HomePageSteps : BaseStepDefinition
    {
        private readonly HelperMethods helperMethods = new HelperMethods();

        #region Given

        [Given(@"the Home Page is displayed for a (.+)")]
        public void GivenTheHomePageIsDisplayedForA(string actorType)
        {
            var loginSteps = Resolve<LoginPageSteps>();

            App.PageBanner.HomeButton.Click();
            App.HomePage.Title.VerifyExists();
            switch (actorType.ToLower())
            {
                case "guest":
                    App.PageBanner.LoginButton.VerifyExists();
                    break;
                case "registered user":
                    GivenThereIsAValidRegisteredUser();
                    loginSteps.GivenTheLoginPageIsDisplayedForAGuest();
                    loginSteps.WhenDetailsForARegisteredUserAreEntered();
                    loginSteps.WhenTheLogInButtonIsClicked();
                    ThenTheHomePageIsDisplayedForARegisteredUser();
                    break;
                default:
                    throw new Exception("no valid test actor specified, please specify if actor is 'guest' or 'registered user'");
            }
        }

        [Given(@"there is a valid registered user")]
        public void GivenThereIsAValidRegisteredUser()
        {
            var passwordSteps = Resolve<PasswordPageSteps>();
            passwordSteps.GivenThePasswordEntryPageIsDisplayedForAGuest();
            passwordSteps.WhenAValidPasswordAndPasswordConfirmationIsEntered();
            passwordSteps.WhenTheCompleteRegistrationButtonIsClicked();
        }

        [Given(@"a registered user has logged into their account at a certain time")]
        public void GivenARegisteredUserHasLoggedIntoTheirAccountAtACertainTime()
        {
            var loginSteps = Resolve<LoginPageSteps>();

            GivenThereIsAValidRegisteredUser();
            loginSteps.GivenTheLoginPageIsDisplayedForAGuest();
            loginSteps.WhenDetailsForARegisteredUserAreEntered();

            helperMethods.EnsureThatEnoughTimeRemainsInTheCurrentInterval();
            string loginSuccessDateTime01 = helperMethods.RecordCurrentDateTimeToWithin10SecondInterval();
            loginSteps.WhenTheLogInButtonIsClicked();
            ScenarioContext.Set(loginSuccessDateTime01, nameof(App.SavedTestData.LoginSuccessDateTime01)); //saved for later in the same scenario
            ThenTheHomePageIsDisplayedForARegisteredUser();
        }

        [Given(@"the registered user logged out of their account")]
        public void GivenTheRegisteredUserLoggedOutOfTheirAccount()
        {
            App.PageBanner.LogoutButton.Click();
            GivenTheHomePageIsDisplayedForA("guest");
        }

        [Given(@"a registered user has re-logged into their account at a certain time")]
        public void GivenARegisteredUserHasRe_LoggedIntoTheirAccountAtACertainTime()
        {
            var loginSteps = Resolve<LoginPageSteps>();

            loginSteps.GivenTheLoginPageIsDisplayedForAGuest();
            loginSteps.WhenDetailsForARegisteredUserAreEntered();

            helperMethods.EnsureThatEnoughTimeHasPassedSinceAPreviouslyRecordedAction();
            string loginSuccessDateTime02 = helperMethods.RecordCurrentDateTimeToWithin10SecondInterval();
            loginSteps.WhenTheLogInButtonIsClicked();
            ScenarioContext.Set(loginSuccessDateTime02, nameof(App.SavedTestData.LoginSuccessDateTime02)); //saved for later in the same scenario
            ThenTheHomePageIsDisplayedForARegisteredUser();
        }

        [Given(@"a registered user fails to log into their account at a certain time")]
        public void GivenARegisteredUserFailsToLogIntoTheirAccountAtACertainTime()
        {
            var loginSteps = Resolve<LoginPageSteps>();

            GivenThereIsAValidRegisteredUser();
            loginSteps.GivenTheLoginPageIsDisplayedForAGuest();
            loginSteps.WhenAValidUserEmailButAnIncorrectPasswordAreEntered();

            helperMethods.EnsureThatEnoughTimeRemainsInTheCurrentInterval();
            string loginFailureDateTime01 = helperMethods.RecordCurrentDateTimeToWithin10SecondInterval();
            loginSteps.WhenTheLogInButtonIsClicked();
            ScenarioContext.Set(loginFailureDateTime01, nameof(App.SavedTestData.LoginFailureDateTime01)); //saved for later in the same scenario
            loginSteps.ThenTheLoginPageIsDisplayed();
        }

        [Given(@"the Login History link is clicked")]
        public void GivenTheLoginHistoryLinkIsClicked()
        {
            WhenTheLoginHistoryLinkIsClicked();
            App.LoginHistoryPage.RecordTable.VerifyExists();
        }

        #endregion

        #region When

        [When(@"the Login link is cicked")]
        public void WhenTheLoginLinkIsCicked()
        {
            App.HomePage.LoginLink.Click();
        }

        [When(@"the Login History link is clicked")]
        public void WhenTheLoginHistoryLinkIsClicked()
        {
            App.HomePage.LoginHistoryLink.Click();
        }

        #endregion

        #region Then

        [Then(@"all home page elements required for a (.+) are present")]
        public void ThenAllHomePageElementsRequiredArePresent(string actorType)
        {
            App.PageBanner.AppName.VerifyExists();
            App.PageBanner.HomeButton.VerifyExists();
            App.HomePage.Title.VerifyExists();
            App.HomePage.InstructionText.VerifyExists();
            switch (actorType.ToLower())
            {
                case "guest":
                    App.PageBanner.RegisterButton.VerifyExists();
                    App.PageBanner.LoginButton.VerifyExists();
                    App.HomePage.InstructionText.VerifyTextContains("Please");
                    App.HomePage.LoginLink.VerifyTextContains("Login");
                    App.HomePage.RegisterLink.VerifyTextContains("Register");
                    break;
                case "registered user":
                    string userEmail = ScenarioContext.Get<string>(nameof(App.SavedTestData.UserEmail));

                    App.PageBanner.LoginHistoryButton.VerifyExists();
                    App.PageBanner.LogoutButton.VerifyExists();
                    App.HomePage.LoggedInText.VerifyTextContains("Hello. You are logged in as");
                    App.HomePage.UserEmailText.VerifyTextContains(userEmail);
                    App.HomePage.LoginHistoryLink.VerifyTextContains("View your login history");
                    App.HomePage.LoginStatsLink.VerifyTextContains("View the login history statistics");
                    break;
                default:
                    throw new Exception("no valid test actor specified, please specify if actor is 'guest' or 'user'");
            }
        }

        [Then(@"the Home Page is displayed for a registered user")]
        public void ThenTheHomePageIsDisplayedForARegisteredUser()
        {
            App.HomePage.LoggedInText.VerifyExists();
        }

        #endregion
    }
}