﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace AcceptanceTests.Steps
{
    [Binding]
    public class PasswordPageSteps :BaseStepDefinition
    {
        #region Given

        [Given(@"the Password Entry page is displayed for a guest")]
        public void GivenThePasswordEntryPageIsDisplayedForAGuest()
        {
            var registerSteps = Resolve<RegisterPageSteps>();

            registerSteps.GivenTheRegisterPageIsDisplayedForAGuest();
            registerSteps.WhenTheRegisterButtonIsClickedWithAValidNameEmailAndDoB();
            ThenThePasswordEntryPageIsDisplayed();
        }

        #endregion

        #region When

        [When(@"an invalid password is entered, that doesn't have (.+)")]
        public void WhenAnInvalidPasswordIsEnteredThatDoesnTHave(string criteria)
        {
            string password;
            switch (criteria.ToLower())
            {
                case "12+ characters":
                    password = "Test1!";
                    break;
                case "1 upper case":
                    password = "testpassword1!";
                    break;
                case "1 lower case":
                    password = "TESTPASSWORD1!";
                    break;
                case "1 number":
                    password = "TestPassword!";
                    break;
                case "1 special character":
                    password = "TestPassword1";
                    break;
                default:
                    throw new Exception("Criteria not recognised, please specify one of the following: '12+ characters', '1 upper case', '1 lower case', '1 number', '1 special character'.");
            }
            App.PasswordEntryPage.PasswordField.EnterText(password);
            App.PasswordEntryPage.ConfirmPasswordField.EnterText(password);
        }

        [When(@"the complete registration button is clicked")]
        public void WhenTheCompleteRegistrationButtonIsClicked()
        {
            App.PasswordEntryPage.CompleteButton.Click();
        }

        [When(@"a valid password and password confirmation is entered")]
        public void WhenAValidPasswordAndPasswordConfirmationIsEntered()
        {
            string userPassword = "MyPassword1!";
            ScenarioContext.Set(userPassword, nameof(App.SavedTestData.UserPassword)); // saved for later use in the same scenario
            App.PasswordEntryPage.PasswordField.EnterText(userPassword);
            App.PasswordEntryPage.ConfirmPasswordField.EnterText(userPassword);
        }

        #endregion

        #region Then

        [Then(@"the Password Entry Page is displayed")]
        public void ThenThePasswordEntryPageIsDisplayed()
        {
            App.PasswordEntryPage.PasswordField.VerifyExists();
        }

        [Then(@"the Password Entry page must contain all required assets")]
        public void ThenThePasswordEntryPageMustContainAllRequiredAssets()
        {
            var password = App.PasswordEntryPage;

            password.Title.VerifyTextContains("Create password");
            password.InstructionText.VerifyTextContains("Please create a password for your new account.");
            password.SpecificationText.VerifyTextContains("Passwords should be at least 12 characters long and contain at least one number, one lowercase character, one uppercase character and one symbol.");
            password.PasswordLabel.VerifyTextContains("Password");
            password.PasswordField.VerifyExists();
            password.ConfirmPasswordLabel.VerifyTextContains("Confirm password");
            password.ConfirmPasswordField.VerifyExists();
            password.CompleteButton.VerifyTextContains("Complete registration");
        }

        [Then(@"an error message is displayed warning that the password does not have (.+)")]
        public void ThenAnErrorMessageIsDisplayedWarningThatThePasswordDoesNotHave(string criteria)
        {
            string errorMessage;
            switch (criteria.ToLower())
            {
                case "12+ characters":
                    errorMessage = "The Password must be at least 12 and at max 100 characters long and contain one number, one lowercase character, one uppercase character and one symbol.";
                    break;
                case "1 upper case":
                    errorMessage = "Passwords must have at least one uppercase ('A'-'Z').";
                    break;
                case "1 lower case":
                    errorMessage = "Passwords must have at least one lowercase ('a'-'z').";
                    break;
                case "1 number":
                    errorMessage = "Passwords must have at least one digit ('0'-'9').";
                    break;
                case "1 special character":
                    errorMessage = "Passwords must have at least one non alphanumeric character.";
                    break;
                default:
                    throw new Exception("Criteria not recognised, please specify one of the following: '12+ characters', '1 upper case', '1 lower case', '1 number', '1 special character'.");
            }
            App.PasswordEntryPage.ValidationSummaryError01.VerifyTextContains(errorMessage);
        }

    #endregion
    }
}
