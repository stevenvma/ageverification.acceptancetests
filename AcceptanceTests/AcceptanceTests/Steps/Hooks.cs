﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using AcceptanceTests.Drivers;

namespace AcceptanceTests.Steps
{
    [Binding]
    public class Hooks : BaseStepDefinition
    {
        [BeforeScenario]
        public void BeforeSceanrio()
        {
            WebDriverFactory.Instance.StartSession();
        }

        [AfterScenario]
        public void CloseDriver()
        {
            WebDriverFactory.Instance.EndSession();
        }
    }
}
