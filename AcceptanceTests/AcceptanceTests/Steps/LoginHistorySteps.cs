﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace AcceptanceTests.Steps
{
    [Binding]
    public class LoginHistorySteps : BaseStepDefinition
    {
        #region Given

        [Given(@"the Login History page is displayed for a registered user")]
        public void GivenTheLoginHistoryPageIsDisplayedForARegisteredUser()
        {
            var homePage = Resolve<HomePageSteps>();

            homePage.GivenTheHomePageIsDisplayedForA("registered user");
            App.HomePage.LoginHistoryLink.Click();
            App.LoginHistoryPage.RecordTable.VerifyExists();
        }

        #endregion

        #region When

        [When(@"the date column header is clicked")]
        public void WhenTheDateColumnHeaderIsClicked()
        {
            App.LoginHistoryPage.DateColumnLabel.Click();
        }

        [When(@"the login history table is filtered for failures")]
        public void WhenTheLoginHistoryTableIsFilteredForFailures()
        {
            App.LoginHistoryPage.FilterButton.Click();
        }

        #endregion

        #region Then

        [Then(@"the Login History page contains all required elements")]
        public void ThenTheLoginHistoryPageContainsAllRequiredElements()
        {
            App.LoginHistoryPage.Title.VerifyTextContains("1 login attempt found");
            App.LoginHistoryPage.RecordTable.VerifyExists();
            //App.LoginHistoryPage.FilterButton.VerifyExists(); // No such element, missed requirement
            App.LoginHistoryPage.DateColumnLabel.VerifyTextContains("Date");
            App.LoginHistoryPage.SuccessColumnLabel.VerifyTextContains("Successful?");
        }

        [Then(@"the Login History page displays the user's first successful login")]
        public void ThenTheLoginHistoryPageDisplaysTheUserSFirstSuccessfulLogin()
        {
            string loginSuccessDateTime01 = ScenarioContext.Get<string>(nameof(App.SavedTestData.LoginSuccessDateTime01));
            App.LoginHistoryPage.RecordDate(2).VerifyTextContains(loginSuccessDateTime01);
            App.LoginHistoryPage.RecordSuccess(2).VerifyTextContains("Success");
        }

        [Then(@"the Login History page displays the user's second successful login")]
        public void ThenTheLoginHistoryPageDisplaysTheUserSSecondSuccessfulLogin()
        {
            string loginSuccessDateTime02 = ScenarioContext.Get<string>(nameof(App.SavedTestData.LoginSuccessDateTime02));
            App.LoginHistoryPage.RecordDate(1).VerifyTextContains(loginSuccessDateTime02);
            App.LoginHistoryPage.RecordSuccess(1).VerifyTextContains("Success");
        }

        [Then(@"the Login History page displayed the user's first failed login")]
        public void ThenTheLoginHistoryPageDisplayedTheUserSFirstFailedLogin()
        {
            string loginFailureDateTime01 = ScenarioContext.Get<string>(nameof(App.SavedTestData.LoginFailureDateTime01));
            App.LoginHistoryPage.RecordDate(2).VerifyTextContains(loginFailureDateTime01);
            App.LoginHistoryPage.RecordSuccess(2).VerifyTextContains("Fail");
        }

        [Then(@"the login history is sorted by date ascending")]
        public void ThenTheLoginHistoryIsSortedByDateAscending()
        {
            string loginSuccessDateTime01 = ScenarioContext.Get<string>(nameof(App.SavedTestData.LoginSuccessDateTime01));
            App.LoginHistoryPage.RecordDate(1).VerifyTextContains(loginSuccessDateTime01); //as opposed to being found in RecordDate(2)
        }

        [Then(@"only login failures are displayed")]
        public void ThenOnlyLoginFailuresAreDisplayed()
        {
            ScenarioContext.Current.Pending();
        }

        #endregion
    }
}
