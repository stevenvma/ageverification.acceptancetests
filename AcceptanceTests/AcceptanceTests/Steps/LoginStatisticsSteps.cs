﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace AcceptanceTests.Steps
{
    [Binding]
    public class LoginStatisticsSteps : BaseStepDefinition
    {
        #region Given

        [Given(@"the Login Statistics Page is displayed for a registered user")]
        public void GivenTheLoginStatisticsPageIsDisplayedForARegisteredUser()
        {
            var homePage = Resolve<HomePageSteps>();

            homePage.GivenTheHomePageIsDisplayedForA("registered user");
            App.HomePage.LoginStatsLink.Click();
            App.LoginStatisticsPage.LoginChart.VerifyExists();
        }

        #endregion

        #region When

        #endregion

        #region Then

        [Then(@"the login statistics page contains all required elements")]
        public void ThenTheLoginStatisticsPageContainsAllRequiredElements()
        {
            App.LoginStatisticsPage.Title.VerifyTextContains("Login History Chart");
            App.LoginStatisticsPage.LoginChart.VerifyExists();
        }

        [Then(@"30 days of login data is displayed, grouped by success or failure")]
        public void Then30DaysOfLoginDataIsDisplayedGroupedBySuccessOrFailure()
        {
            throw new Exception("The contents of the login statistics chart or not exposed - it is not possible to test them using the automation framework");
        }


        #endregion
    }
}
